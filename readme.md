#Configuration example (alpha)#

```
#!yml

jb_copy_to:
    rules:
        fontawesome:
            input: "%kernel.root_dir%/../vendor/bower/fontawesome/fonts/*"
            output: "%kernel.root_dir%/../web/fonts"
```