<?php

namespace Jb\CopyToBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Finder\Finder;

class CopyToCommand extends ContainerAwareCommand {

    protected $fs;

    protected function configure() {
        $this
            ->setName('jb:copyto:copy')
            ->setDescription('Copy file following rules')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->fs = $this->getContainer()->get('filesystem');
        $ruleCollection = $this->getContainer()->getParameter('jb_copy_to.rules');
	
        if(!$input->getOption('force')) {
            $output->writeln('<error>[ERROR] please use --force option</error>');
            return;
        }

        foreach ($ruleCollection as $name => $attributes) {
            $toPath = $attributes['output']['path'];
            $toType = $attributes['output']['type'];

            if($toType == 'dir') {
                $this->createDir($toPath, $output);
            }else {
                $dirname = dirname($toPath);
                $this->createDir($dirname, $output);
            }
            
            foreach ($attributes['input'] as $fromPath) {
                if(is_dir($fromPath)) {
                    if(!is_dir($toPath)) {
                        $output->writeln('<error>[ERROR] If from path is a directory, to path must be a directory</error>');
                        continue;
                    }
                    $this->fs->mirror($fromPath, $toPath, Finder::create()->ignoreDotFiles(true)->in($fromPath));
                    $output->writeln('<info>[MIRRORING] '.$fromPath.' -> '.$toPath.'</info>');
                }else if(is_file($fromPath)) {
                    if($toType == 'dir') {
                        $toPath = $toPath.'/'.basename($fromPath);
                        $this->touchFile($toPath);
                    }
                    $this->fs->copy($fromPath, $toPath, true);
                    $output->writeln('<info>[FILE+] '.$toPath.'</info>');
                }else {
                    $filesCollection = glob($fromPath);
                    if(count($filesCollection) && $toType == 'file') {
                        $output->writeln('<error>[ERROR] If from path is a pattern, to path must be a directory</error>');
                    }
                    $elementMoved = 0; 
                    foreach ($filesCollection as $file) {
                        if($file == ".." || $file == ".") {
                            continue;
                        }
                        $toPathBuild = $toPath.'/'.basename($file);
                        $this->touchFile($toPathBuild);
                        $this->fs->copy($file, $toPathBuild, true);
                        $output->writeln('<info>[FILE+] '.$toPathBuild.'</info>');
                        $elementMoved++;
                    }
                    if($elementMoved === 0) {
                        $output->writeln('<error>[ERROR] 0 element moved for : '.$fromPath.'</error>');
                    }
                }
            }
        }
    }

    protected function createDir($to, $output = null, $chmod = 0775){
        /*if($this->fs->exists($to)) {
            $this->fs->remove($to);
            if($output !== null){
                $output->writeln('<comment>[DIR-] '.$to.'</comment>');
            }         
        }*/
        if(!($this->fs->exists($to))) {
            $this->fs->mkdir($to, $chmod);
            if($output !== null){
                $output->writeln('<info>[DIR+] '.$to.'</info>');
            }
        }
    }

    protected function touchFile($to, $output = null, $chmod = 0664){
        if(!($this->fs->exists($to))) {
            $this->fs->touch($to, $chmod);
            if($output !== null){
                $output->writeln('<info>[FILE+] '.$to.'</info>');
            }
        }
    }
}
