<?php

namespace Jb\CopyToBundle\CopyClass;

use Jb\CopyToBundle\CopyClass\AbstractCopyTo;

class Filesystem extends AbstractCopyTo {

    public function copy($from, $to, $context = array()) {
        if(is_file($from)){
            $this->createDirectories($to);
            $status = @copy($from, $to);
            $return = array(array("from" => $from, "to" => $to, "status" => $status));
        }else{
            $files = glob($from);

            if($to[strlen($to)-1] != '/') {
                $to .= '/';
            }

            $return = array();

            foreach ($files as $file) {
                $toPath = sprintf('%s%s', $to, basename($file));
                $this->createDirectories($toPath);
                $status = @copy($file, $toPath);
                $return[] = array("from" => $file, "to" => $toPath, "status" => $status);
            }
        }

        return $return;
    }

}
